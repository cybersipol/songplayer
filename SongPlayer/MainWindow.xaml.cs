﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace SongPlayer
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> songPaths = new List<string>();

        public MainWindow()
        {
            InitializeComponent();            
        }


        // ładowanie listy plików
        private void CommandLoadList( object sender, RoutedEventArgs e )
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            string defaultDir = Properties.Settings.Default.DefaultDir ?? String.Empty;
            if (String.IsNullOrWhiteSpace(defaultDir) == false)
                openFileDlg.InitialDirectory = defaultDir;

            openFileDlg.Filter = "MP3 Files|*.mp3|Wave files|*.wav";

            //Kod do wybrania wielu plików
            openFileDlg.Multiselect = true;

            bool? result = openFileDlg.ShowDialog();

            if ((result ?? false) == false)
                return;

            //Zapisuje ścieżki utworow w do listy
            songPaths = (openFileDlg.FileNames ?? new string[] { }).ToList();

            foreach (string path in songPaths)
                ListBoxSongs.Items.Add(Path.GetFileNameWithoutExtension(path));

            // Zapamietuje, w ustawieniach uzytkownika systemu, katalog ostatniego pliku
            if (songPaths != null && songPaths.Count > 0)
                Properties.Settings.Default.DefaultDir = Path.GetDirectoryName(songPaths.Last());

        }


        private void Window_Closed(object sender, EventArgs e)
        {
            kamaSongs.Stop();
            // Przy zamknięciu zapisz ustawienia uzytkownika systemu
            Properties.Settings.Default.Save();
        }

        private void ListBoxSongs_SelectionChanged( object sender, System.Windows.Controls.SelectionChangedEventArgs e )
        {
            int index = ListBoxSongs.SelectedIndex;
            if (index >= 0 && index < songPaths.Count)
                kamaSongs.LoadAudioFile(songPaths[index]);
            else
                kamaSongs.LoadAudioFile(null);

        }

    }

}