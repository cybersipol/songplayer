﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SongPlayer
{
    /// <summary>
    /// Logika interakcji dla klasy KamaAudioPlayer.xaml
    /// </summary>
    public partial class KamaAudioPlayer : UserControl, INotifyPropertyChanged
    {
        private DispatcherTimer Timer;
        private string _lastSource = null;

        public KamaAudioPlayer()
        {
            InitializeComponent();

            SetMediaElementsDefaults();

            MakeTimer();
        }

        // zwraca osoatnie uzyte źródło MediaElement
        public string LastAudioSource => _lastSource ?? String.Empty;

        // zwraca nazwę pliku (bez katalogu i rozszerzenia) z ostatniego źródła MediaElement
        public string AudioName => Path.GetFileNameWithoutExtension(LastAudioSource);
        
        /// <summary>
        /// ustawia zródło muzyki (pełna ścieżka dostepu do pliku audio)  
        /// </summary>
        public string AudioSource
        {
            get => Songs.Source?.ToString() ?? String.Empty;
            set => SetAudioSource(value);
        }

        public string CurrentTime
        {
            get => Songs.Position.ToString(@"hh\:mm\:ss");
            set => OnPropertyChanged(nameof(CurrentTime));
        }

        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// ładuje plik do grania 
        /// </summary>
        /// <param name="filepath">pełna ścieżka do pliku audio</param>
        public void LoadAudioFile( string filepath ) => AudioSource = filepath;


        // Zatrzymuje granie, ustawia prametry wewnetrznych komponentów 
        public void Stop()
        {
            Songs.Stop();
            Timer.Stop();
            ProgressScroll.Value = 0d;
            // powiadom iz właściwość CurrentTime się zmieniła
            OnPropertyChanged(nameof(CurrentTime));
        }

        // Rozpoczyna granie utworu
        public void Play()
        {
            Songs.Play();
            Timer.Start();
            // powiadom iz właściwość CurrentTime się zmieniła
            OnPropertyChanged(nameof(CurrentTime));
        }

        // wstrzymuje granie utworu
        public void Pause()
        {
            Songs.Pause();
            Timer.Stop();
            // powiadom iz właściwość CurrentTime się zmieniła
            OnPropertyChanged(nameof(CurrentTime));
        }

        #region private funcs

        // metoda pomocnicza ustawiajaca źródło w MediElement i zapmiętujaca ostatnią ścieżkę ustawienia
        private void SetAudioSource(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                Songs.Source = null;
            else
                Songs.Source = new Uri(value, UriKind.RelativeOrAbsolute);

            _lastSource = value;

            // powiadom że właściwości się zmieniły
            OnPropertyChanged(nameof(AudioSource));
            OnPropertyChanged(nameof(AudioName));
            OnPropertyChanged(nameof(LastAudioSource));

            // zatrzymaj granie utworu
            Stop();
        }


        // Powiadamia iż jakaś właściwość się zmieniała (potrzebne w WPF do bieżącego odświeżanaia danych)
        private void OnPropertyChanged( string propertyName ) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        // zdarzenie klikniecia przycisku Play
        private void CommandPlay( object sender, RoutedEventArgs e ) => Play();

        // zdarzenie klikniecia przycisku Pause
        private void CommandPause( object sender, RoutedEventArgs e ) => Pause();

        // zdarzenie klikniecia przycisku Stop
        private void CommandStop( object sender, RoutedEventArgs e ) => Stop();



        // zdarzenie zakończenia przesuwania suwaka z czasem utworu
        private void ProgressScroll_DragCompleted( object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e ) => Play();

        // zdarzenie rozpoczeńcia przesuwania suwaka z czasem utworu
        private void ProgressScroll_DragStarted( object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e ) => Pause();


        // zdarzenie zmiany wartości suwaka z czasem utworu
        private void ProgressScroll_ValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e )
        {
            Songs.Position = TimeSpan.FromSeconds(ProgressScroll.Value);            
        }

        // zdarzenie kiedy zachodzi przesuwanie suwaka z czasem utworu
        private void ProgressScroll_DragDelta( object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e )
        {
            Songs.Position = TimeSpan.FromSeconds(ProgressScroll.Value);
            OnPropertyChanged(nameof(CurrentTime));
        }


        // Zdarzenie zachodzące po załadowaniu komponentu
        private void KamaAudioPlayer_Loaded( object sender, RoutedEventArgs e )
        {
            Songs.ScrubbingEnabled = false;
            Songs.Stop();

            Songs.MediaOpened -= Songs_MediaOpened;
            Songs.MediaOpened += Songs_MediaOpened;
        }

        // zdarzenie zachodzące po otworzeniu pliku audio
        private void Songs_MediaOpened( object sender, RoutedEventArgs e )
        {
            ProgressScroll.Maximum = Songs.NaturalDuration.TimeSpan.TotalSeconds;
            TotalTimeSong.Content = Songs.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss");
        }

        // zdarzenie czasomierza (Timer)
        private void Timer_Tick( object sender, EventArgs e )
        {
            ProgressScroll.Value = Songs.Position.TotalSeconds;
            OnPropertyChanged(nameof(CurrentTime));
        }

        // ustawia domyślne parametry dla elementu WPF MediaElement
        private void SetMediaElementsDefaults()
        {
            Songs.LoadedBehavior = MediaState.Manual;
            Songs.Volume = 0.5d;
            Songs.IsMuted = false;
        }

        // tworzenie i ustawienie domyślne czasomierza
        private void MakeTimer()
        {
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromSeconds(1);
            Timer.Tick -= Timer_Tick;
            Timer.Tick += Timer_Tick;
        }

        #endregion

    }
}
